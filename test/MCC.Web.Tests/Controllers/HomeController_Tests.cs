﻿using System.Threading.Tasks;
using MCC.Web.Controllers;
using Shouldly;
using Xunit;

namespace MCC.Web.Tests.Controllers
{
    public class HomeController_Tests: MCCWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}
