using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MCC.Web.Startup;
namespace MCC.Web.Tests
{
    [DependsOn(
        typeof(MCCWebModule),
        typeof(AbpAspNetCoreTestBaseModule)
        )]
    public class MCCWebTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCWebTestModule).GetAssembly());
        }
    }
}