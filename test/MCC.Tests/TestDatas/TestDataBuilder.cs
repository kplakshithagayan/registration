using MCC.EntityFrameworkCore;

namespace MCC.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly MCCDbContext _context;

        public TestDataBuilder(MCCDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //create test data here...
        }
    }
}