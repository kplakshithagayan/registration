﻿using System;
using System.Threading.Tasks;
using Abp.TestBase;
using MCC.EntityFrameworkCore;
using MCC.Tests.TestDatas;

namespace MCC.Tests
{
    public class MCCTestBase : AbpIntegratedTestBase<MCCTestModule>
    {
        public MCCTestBase()
        {
            UsingDbContext(context => new TestDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<MCCDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<MCCDbContext>())
            {
                action(context);
                context.SaveChanges();
            }
        }

        protected virtual T UsingDbContext<T>(Func<MCCDbContext, T> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<MCCDbContext>())
            {
                result = func(context);
                context.SaveChanges();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<MCCDbContext, Task> action)
        {
            using (var context = LocalIocManager.Resolve<MCCDbContext>())
            {
                await action(context);
                await context.SaveChangesAsync(true);
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<MCCDbContext, Task<T>> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<MCCDbContext>())
            {
                result = await func(context);
                context.SaveChanges();
            }

            return result;
        }
    }
}
