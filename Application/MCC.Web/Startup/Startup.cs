﻿using System;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Abp.EntityFrameworkCore;
using MCC.EntityFrameworkCore;
using Castle.Facilities.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CFS.Web.Host.Startup;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using System.Linq; 
using Abp.Extensions;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using MCC.Web.Host.Configuration;
using CFS.Web.Host.Configuration;

namespace MCC.Web.Startup
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhost";
        private readonly IConfigurationRoot _appConfiguration;

        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            configuration.RegisterAppSettings();
            _appConfiguration = env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //Configure DbContext
            services.AddAbpDbContext<MCCDbContext>(options =>
            {
                DbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });

            services.AddMvc(
                 options => options.Filters.Add(new CorsAuthorizationFilterFactory(_defaultCorsPolicyName))
             );

           // services.AddCors(
           //    options => options.AddPolicy(
           //        _defaultCorsPolicyName,
           //        builder => builder
           //            .WithOrigins(
           //                // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
           //                _appConfiguration["App:CorsOrigins"]
           //                    .Split(",", StringSplitOptions.RemoveEmptyEntries)
           //                    .Select(o => o.RemovePostFix("/"))
           //                    .ToArray()
           //            )
           //            .AllowAnyHeader()
           //            .AllowAnyMethod()
           //            .AllowCredentials()
           //    )
           //);

            // Swagger - Enable this line and the related lines in Configure method to enable swagger UI
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "WWWQQQ API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });


            services.AddMvc();

            //Configure Abp and Dependency Injection
            return services.AddAbp<MCCHostModule>(options =>
            {
                //Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; }); // Initializes ABP framework.
            app.UseCors(_defaultCorsPolicyName); // Enable CORS!
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller}/{action}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=home}/{action=api}/{id?}");
            });

            app.UseCors(builder => builder.WithOrigins(new[] { "http://localhost:8000" }));
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("http://localhost:5000/swagger/v1/swagger.json", "CFS API V1");
                options.IndexStream = () => Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("MCC.Web.wwwroot.swagger.ui.index.html");
            }); // URL: /swagger
        }
    }
}
