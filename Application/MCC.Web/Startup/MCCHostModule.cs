﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using CFS.Web.Host.Configuration;
using MCC.Api; 
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace CFS.Web.Host.Startup
{
    [DependsOn(
       typeof(MCCServiceModule))]
    public class MCCHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MCCHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCHostModule).GetAssembly());
        }
    }
}
