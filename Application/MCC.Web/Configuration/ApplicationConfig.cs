﻿using Inx.Utility;
using Microsoft.Extensions.Configuration;

namespace MCC.Web.Host.Configuration
{
    public static class ApplicationConfig
    {
        public static void RegisterAppSettings(this IConfiguration config)
        {
            GlobalConfig.ConnectionString = config["ConnectionStrings:Default"];
        }
    }
}
