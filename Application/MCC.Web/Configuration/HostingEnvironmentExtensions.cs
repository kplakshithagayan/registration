﻿using MCC.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;


namespace CFS.Web.Host.Configuration
{
    public static class HostingEnvironmentExtensions
    {
        public static IConfigurationRoot GetAppConfiguration(this IHostingEnvironment env)
        {
            return AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }
    }
}
