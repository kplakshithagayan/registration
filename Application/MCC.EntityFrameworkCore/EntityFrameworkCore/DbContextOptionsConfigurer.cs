﻿using Microsoft.EntityFrameworkCore;

namespace MCC.EntityFrameworkCore
{
    public static class DbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<MCCDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for MCCDbContext */
            dbContextOptions.UseSqlServer(connectionString);
        }
    }
}
