﻿using MCC.Configuration;
using MCC.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MCC.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class MCCDbContextFactory : IDesignTimeDbContextFactory<MCCDbContext>
    {
        public MCCDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MCCDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(MCCConsts.ConnectionStringName)
            );

            return new MCCDbContext(builder.Options);
        }
    }
}