﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MCC.EntityFrameworkCore
{
    public class MCCDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...

        public MCCDbContext(DbContextOptions<MCCDbContext> options) 
            : base(options)
        {

        }
    }
}
