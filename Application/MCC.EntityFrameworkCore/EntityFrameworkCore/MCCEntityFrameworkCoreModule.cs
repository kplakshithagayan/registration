﻿using Abp.EntityFrameworkCore;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MCC.EntityFrameworkCore
{
    [DependsOn(
        typeof(MCCCoreModule), 
        typeof(AbpEntityFrameworkCoreModule))]
    public class MCCEntityFrameworkCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCEntityFrameworkCoreModule).GetAssembly());
        }
    }
}