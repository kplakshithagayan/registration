﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using MCC.Localization;

namespace MCC
{
    public class MCCCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            MCCLocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCCoreModule).GetAssembly());
        }
    }
}