﻿using Abp.Dependency; 

namespace CFS.Api.Utility
{
    public class IocConfig
    {
        public static void Register(IIocManager iocManager)
        {
            //#region comman
            //iocManager.RegisterIfNot<IMemoryCacheManager, MemoryCacheManager>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IBaseInjector, BaseInjector>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<ICoreInjector, CoreInjector>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IMessageService<GoogleSmtpConfig>, GoogleClient>(DependencyLifeStyle.Transient);
            //#endregion

            //iocManager.RegisterIfNot<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            //iocManager.RegisterIfNot<IProfileService, ProfileService>();
            //iocManager.RegisterIfNot<IUserManageAppService, UserManageAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IAgentAppService, AgentAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IAgentUserAppService, AgentUserAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<ICompanyUserAppService, CompanyUserAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IAuthenticationAppService, AuthenticationAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<ISharedKernelAppService, SharedKernelAppService>(DependencyLifeStyle.Transient);
            //iocManager.RegisterIfNot<IPermissionAppService, PermissionAppService>(DependencyLifeStyle.Transient);
            //RegisterRepositories(iocManager);
        }

        static void RegisterRepositories(IIocManager iocManager)
        {
        //    iocManager.RegisterIfNot<IAppRepository<Domain.Agent.Agent, int>, AppRepository<Domain.Agent.Agent, int>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<Domain.Agent.AgentUser, long>, AppRepository<Domain.Agent.AgentUser, long>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<CompanyUser, long>, AppRepository<CompanyUser, long>>(DependencyLifeStyle.Transient);

        //    iocManager.RegisterIfNot<IAppRepository<User, long>, AppRepository<User, long>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<UserPermission, int>, AppRepository<UserPermission, int>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<Permission, int>, AppRepository<Permission, int>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<PermissionUserType, int>, AppRepository<PermissionUserType, int>>(DependencyLifeStyle.Transient);
        //    iocManager.RegisterIfNot<IAppRepository<County, int>, AppRepository<County, int>>(DependencyLifeStyle.Transient);
        }
    }
}
