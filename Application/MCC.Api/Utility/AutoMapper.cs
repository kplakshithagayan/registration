﻿using Abp.AutoMapper;
using Abp.Configuration.Startup; 

namespace MCC.Api.Utility
{
    public class Automapper
    {
        public static void Register(IAbpStartupConfiguration Configuration)
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                //cfg.CreateMap<AgentModel, AgentDto>().ReverseMap();
                //cfg.CreateMap<AgentDto, Agent>().ReverseMap();
                //cfg.CreateMap<AgentDto, AgentOperationDto>().ReverseMap();
                //cfg.CreateMap<AgentModel, AgentOperationModel>().ReverseMap();
                //cfg.CreateMap<AgentUserModel, AgentUserDto>().ReverseMap();


                //cfg.CreateMap<AgentUserDto, AgentUser>().ReverseMap()
                //    .ForPath(src => src.AccountStatus, dest => dest.MapFrom(src => src.User.AccountStatus))
                //    .ForPath(src => src.IsAdmin, dest => dest.MapFrom(src => src.User.IsAdmin));

                //cfg.CreateMap<AgentViewResultModel, AgentViewResultDto>().ReverseMap()
                //.ForMember(src => src.Email, dest => dest.MapFrom(src => src.Email ?? string.Empty));
                //cfg.CreateMap<CompanyUserViewResult, CompanyUserViewResultDto>().ReverseMap();
                //cfg.CreateMap<CompanyUser, CompanyUserViewResultDto>().ReverseMap();
                //cfg.CreateMap<CompanyUserModel, CompanyUserDto>().ReverseMap();
                //cfg.CreateMap<CompanyUserDto, CompanyUser>().ReverseMap();
                //cfg.CreateMap<UserRegisterDto, User>().ReverseMap();
                //cfg.CreateMap<User, UserDto>().ReverseMap();
                //cfg.CreateMap<CFS.Domain.Comman.ValueObject.Address, CFS.Model.Comman.Address>().ReverseMap();

            });
        }
    }
}

