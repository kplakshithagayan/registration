﻿using Abp.Modules;
using Abp.AspNetCore;
using MCC.EntityFrameworkCore;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using CFS.Api.Utility;
using Abp.Reflection.Extensions;
using MCC.Api.Utility;

namespace MCC.Api
{
    [DependsOn(typeof(AbpAspNetCoreModule),
         typeof(MCCApplicationModule),
         typeof(MCCEntityFrameworkCoreModule),
         typeof(AbpAutoMapperModule))]
    public class MCCServiceModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MCCServiceModule(IHostingEnvironment env)
        {
        }
        public override void PreInitialize()
        {
            IocConfig.Register(IocManager);
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCServiceModule).GetAssembly());
            Automapper.Register(Configuration);
        }
    }
}
