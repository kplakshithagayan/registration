﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MCC
{
    [DependsOn(
        typeof(MCCCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MCCApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MCCApplicationModule).GetAssembly());
        }
    }
}