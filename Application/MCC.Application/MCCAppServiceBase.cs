﻿using Abp.Application.Services;

namespace MCC
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class MCCAppServiceBase : ApplicationService
    {
        protected MCCAppServiceBase()
        {
            LocalizationSourceName = MCCConsts.LocalizationSourceName;
        }
    }
}