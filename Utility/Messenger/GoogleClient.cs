﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Inx.Utility.Exceptions;

namespace Inx.Utility.Messenger
{
    public class GoogleClient : IMessageService<GoogleSmtpConfig>
    {
        public void Send(EmailMessage message, GoogleSmtpConfig config)
        {
            try
            {
                var from = config.FromEmailAddress;
                var password = config.FromEmailPassword;
                var client = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(from, password),
                    Timeout = 10000,
                };
                var mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(from, "CFS Zipp");
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = message.Subject;
                mailMessage.Body = message.Body;

                foreach (var item in message.ToMails ?? new List<string>())
                {
                    mailMessage.To.Add(item);
                }
                client.Send(mailMessage);
            }
            catch (Exception e)
            {
                throw new EatException(e);
            }
        }
    }
}
