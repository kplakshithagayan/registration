﻿using System.Collections.Generic;
using System.Net.Mail;
namespace Inx.Utility.Messenger
{
    public class EmailMessage
    {
        public string Body { get; set; }
        public List<string> ToMails { get; set; }
        public string Subject { get; set; }
        public List<Attachment> Attachments { get; set; }  
        public EmailMessage()
        {
            ToMails = new List<string>();
            Attachments = new List<Attachment>();
        }
    }
}
