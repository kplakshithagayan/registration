﻿namespace Inx.Utility.Messenger
{
    public class GoogleSmtpConfig
    {
        public string FromEmailAddress { get; set; }
        public string FromEmailPassword { get; set; }
    }
}
