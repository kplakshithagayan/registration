﻿using System.Threading.Tasks;

namespace Inx.Utility.Messenger
{
    public interface IMessageService<T>
    {
        void Send(EmailMessage message,T config);
    }
}
