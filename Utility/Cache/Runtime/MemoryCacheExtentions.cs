﻿namespace Inx.Utility.Cache.Runtime
{
    public static class MemoryCacheExtentions
    {
        public static T CacheSetAndGet<T>(this T item, IMemoryCacheManager cache,string key)
        {
            cache.SetAsync(key, item);
            return item;
        }
    }
}
