﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Inx.Utility.Cache.Runtime
{
    public interface IMemoryCacheManager
    {
        Task<T> GetAsync<T>(string key);
        T Get<T>(string key);
        Task SetAsync(string key, object data, int cacheTime = int.MaxValue);
        void Set(string key, object data, int cacheTime = int.MaxValue);
        Task<bool> IsSetAsync(string key);
        bool IsSet(string key);

        Task RemoveAsync(string key);
        Task RemoveAsync(List<string> keys);
        void Remove(List<string> keys);
        void Remove(string key);
        Task RemoveByStartWithAsync(string pattern);
        Task RemoveByStartWithAsync(List<string> patterns);
        void RemoveByStartWith(string pattern);
        void RemoveByStartWith(List<string> patterns);

        Task Clear();
        Task<T> GetAsync<T>(string key, Func<T> func);
    }
}
