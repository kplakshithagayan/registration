﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Abp.Runtime.Caching;
using Inx.Utility.Extentions;
namespace Inx.Utility.Cache.Runtime
{
    public class MemoryCacheManager : IMemoryCacheManager
    {
        private readonly ICacheManager _cacheManager;
        const string ckey = "cachekeys";
        public MemoryCacheManager(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        public T Get<T>(string key)
        {
            try
            {
                return (T)(_cacheManager
                    .GetCache("CFS")
                    .GetOrDefault(key));
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        public async Task<T> GetAsync<T>(string key)
        {
            try
            {
                return (T)(await _cacheManager
                    .GetCache("FCS")
                    .GetOrDefaultAsync(key));
            }
            catch (Exception e)
            {
                return default(T);
            }

        }
        public async Task<T> GetAsync<T>(string key, Func<T> func)
        {
            try
            {
                var item = (T)(await _cacheManager
                    .GetCache("FCS")
                    .GetOrDefaultAsync(key));
                if (!item.IsNull()) return item;
                var result = func();
                await SetAsync(key, result);
                return result;
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        public async Task SetAsync(string key, object data, int cacheTime = int.MaxValue)
        {
            try
            {
                await _cacheManager
                    .GetCache("FCS")
                    .SetAsync(key, data);
                await UpdateKeyPoolAsync(key);
            }
            catch (Exception e)
            {
                //todo logg exception cache
            }
        }
        public void Set(string key, object data, int cacheTime = int.MaxValue)
        {
            try
            {
                _cacheManager
                    .GetCache("FCS")
                    .Set(key, data);
                UpdateKeyPoolAsync(key).GetAwaiter();
            }
            catch (Exception e)
            {  //todo logg exception cache
            }


        }

        public async Task<bool> IsSetAsync(string key)
        {
            try
            {
                return (await _cacheManager
                           .GetCache("FCS")
                           .GetOrDefaultAsync(key)) != null;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        public bool IsSet(string key)
        {
            try
            {
                return (_cacheManager
                           .GetCache("FCS")
                           .GetOrDefault(key)) != null;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task RemoveAsync(List<string> keys)
        {
            foreach (var item in keys)
            {
                await RemoveAsync(item);
                await UpdateKeyPoolAsync(item, false);
            }
        }
        public async Task RemoveAsync(string key)
        {
            try
            {
                await _cacheManager
                    .GetCache("FCS")
                    .RemoveAsync(key);
                await UpdateKeyPoolAsync(key, false);
            }
            catch (Exception e)
            {
                //todo logg exception
            }
        }
        public void Remove(List<string> keys)
        {
            foreach (var item in keys)
            {
                Remove(item);
                UpdateKeyPool(item, false);
            }
        }
        public void Remove(string key)
        {
            try
            {
                _cacheManager
                    .GetCache("FCS")
                    .Remove(key);
                UpdateKeyPool(key, false);
            }
            catch (Exception e)
            {
                //todo logg exception
            }
        }

        //https://github.com/aspnetboilerplate/aspnetboilerplate/issues/2060
        public async Task RemoveByStartWithAsync(string pattern)
        {
            try
            {
                var keys = (List<string>)await _cacheManager.GetCache("FCS").GetOrDefaultAsync(ckey)
                           ?? new List<string>();
                var clearcache = (from item in keys where item.StartsWith(pattern, StringComparison.OrdinalIgnoreCase) select item).ToList();
                foreach (var item in clearcache)
                {
                    await RemoveAsync(item);
                    await UpdateKeyPoolAsync(item, false);
                }
            }
            catch (Exception e)
            {
                //logg exception
            }
        }
        public async Task RemoveByStartWithAsync(List<string> patterns)
        {
            try
            {
                foreach (var pattern in patterns)
                {
                    await RemoveByStartWithAsync(pattern);
                }
            }
            catch (Exception e)
            {
                //logg exception
            }
        }
        public void RemoveByStartWith(string pattern)
        {
            try
            {
                var keys = (List<string>)_cacheManager.GetCache("FCS").GetOrDefault(ckey)
                           ?? new List<string>();
                var clearcache = (from item in keys where item.StartsWith(pattern, StringComparison.OrdinalIgnoreCase) select item).ToList();
                foreach (var item in clearcache)
                {
                    Remove(item);
                    UpdateKeyPool(item, false);
                }
            }
            catch (Exception e)
            {
                //logg exception
            }
        }
        public void RemoveByStartWith(List<string> patterns)
        {
            try
            {
                foreach (var pattern in patterns)
                {
                    RemoveByStartWith(pattern);
                }
            }
            catch (Exception e)
            {
                //logg exception
            }
        }


        public async Task Clear()
        {
            await _cacheManager
                   .GetCache("FCS").ClearAsync();
        }

        #region private methods

        [Description("isadd=false ==> remove")]
        private async Task UpdateKeyPoolAsync(string key, bool isAdd = true)
        {
            try
            {
                var keys = (List<string>)await _cacheManager.GetCache("FCS").GetOrDefaultAsync(ckey)
                           ?? new List<string>();
                var isKeyThere = keys.Contains(key);
                if (isKeyThere && isAdd)
                {
                    return;
                }
                else if (!isKeyThere && isAdd)
                {
                    keys.Add(key);
                }
                else
                {
                    keys.Remove(key);
                }

                await _cacheManager
                    .GetCache("FCS")
                    .SetAsync(ckey, keys);
            }
            catch (Exception e)
            {
                //todo logg exception cache
            }
        }
        [Description("isadd=false ==> remove")]
        private void UpdateKeyPool(string key, bool isAdd = true)
        {
            try
            {
                var keys = (List<string>)_cacheManager.GetCache("FCS").GetOrDefault(ckey)
                           ?? new List<string>();
                var isKeyThere = keys.Contains(key);
                if (isKeyThere && isAdd)
                {
                    return;
                }
                else if (!isKeyThere && isAdd)
                {
                    keys.Add(key);
                }
                else
                {
                    keys.Remove(key);
                }
                _cacheManager
                  .GetCache("FCS")
                  .Set(ckey, keys);
            }
            catch (Exception e)
            {
                //todo logg exception cache
            }
        }
        #endregion

    }
}
