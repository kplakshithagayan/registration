﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Inx.Utility.Extentions
{
    public static class PattenExtentions
    {
        [Description("1,1,1,2,1,2==>[1,2][2,1]")]
        public static List<int[]> DistinctPairFinder(this int[] a)
        {
            var arrayList = new List<int[]>();
            int i, k;
            var ht1 = new Hashtable();
            var ht2 = new Hashtable();
            var arrayLength = a.Length;

            for (i = 0; i < arrayLength; i++)
            {
                if (!ht1.ContainsValue(a[i]))
                {
                    ht1.Add(i, a[i]);

                    for (k = i + 1; k < arrayLength; k++)
                    {
                        if (!ht2.ContainsValue(a[k]) && a[i] != a[k])
                        {
                            ht2.Add(k, a[k]);
                            arrayList.Add(new int[] { a[i], a[k] });
                        }
                    }
                    ht2.Clear();
                }
            }
            return arrayList;
        }

        public static int[] RemoveDuplicates(this int[] a)
        {
            var result = new List<int>();
            for (var i = 0; i < a.Length; i++)
            {
                if (i == 0)
                {
                    result.Add(a[i]);
                }
                else
                {
                    if (a[i - 1] != a[i])
                    {
                        result.Add(a[i]);
                    }
                }
            }
            return result.ToArray();
        }
    }
}
