﻿using System;
using System.Collections.Generic;
using Inx.Utility.Exceptions;

namespace Inx.Utility.Extentions
{
    public static class StructExtention
    {
        public static bool IsZero(this int value)
        {
            return value == 0;
        }
        public static bool IsZero(this decimal value)
        {
            return value == 0;
        }
        public static bool Is(this int value,int compare)
        {
            return value == compare;
        }
        public static int MustNotZero(this int value)
        {
            if (value==0)
            {
                throw new UnAuthorizedException("tenant must be not zero");
            }
            return value;
        }

        public static DateTime IntToDateTime(this int dateInt,DateTime date)
        {
            var s = dateInt.IntToDateTime();
            return new DateTime(s.Year, s.Month, s.Day, date.Hour, date.Minute, date.Second);
        }

        public static bool Content(this int value,List<int> items)
        {
            return items.Contains(value);
        }

        public static decimal ToPrice(this decimal value)
        {
            return Math.Round(value, 2);
        }

        public static bool ComparePrice(this decimal value, decimal comparewith)
        {
            return Math.Round(value, 2) == Math.Round(comparewith, 2);
        }

        public static decimal ToActualPrice(this decimal value)
        {
            return value / 100;
        }

        public static decimal ApplyPercentage(this decimal value, decimal percentage)
        {
            return value * (percentage / 100);
        }
    }
}
