﻿using Abp.Dependency;
namespace Inx.Utility.Extentions
{
    public static class AbpExtention
    {
        public static void RegisterIfNot<TType, TImpl>(this IIocManager iocManager) where TType : class
            where TImpl : class, TType
        {
            if (!iocManager.IsRegistered<TType>())
            {
                iocManager.Register<TType, TImpl>(DependencyLifeStyle.Transient);
            }
         
        }
    }
}
