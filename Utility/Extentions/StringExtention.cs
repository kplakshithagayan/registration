﻿using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Inx.Utility.Exceptions;
using Newtonsoft.Json;

namespace Inx.Utility.Extentions
{
    public static class StringExtention
    {
        static string[] extentions = {
            "doc", "docx", "pdf", "txt", "csv", "ppt", "rar", "html", "htm", "zip", "xls", "xlsx",
        };
        static string[] imageextentions = {
            "jpeg", "jpg", "gif", "png",
        };
        public static string TrimAndToLower(this string value)
        {
            return value?.Trim().ToLower() ?? "";
        }
        public static string RemoveMiddleSpaces(this string value)
        {
            var charx = value.ToCharArray();
            return charx.Aggregate(string.Empty, (current, item) => current + (item.ToString() == " " ? "" : item.ToString()));
        }
        public static bool IsAsc(this string value)
        {
            return value.TrimAndToLower() == "asc";
        }
        public static bool ValiedJson(this string value)
        {
            try
            {
                JsonConvert.DeserializeObject<dynamic>(value);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static T ToJsonObject<T>(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(value);

        }
        public static T ToJsonObject<T>(this string value, bool ignoreErrors)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(value);
            }
            catch (Exception e)
            {
                return default(T);
            }

        }
        public static Guid ToValiedGuid(this string value)
        {
            var token = Guid.Empty;
            if (!Guid.TryParse(value, out token))
            {
                throw new RecordNotFoundException();
            }
            if (token == Guid.Empty)
            {
                throw new RecordNotFoundException();
            }
            return token;
        }

        public static string ChangeFirstLetterCase(this string value, bool isCapital = false)
        {
            if (!string.IsNullOrEmpty(value) && value.Length > 0)
            {
                return (isCapital ? value[0].ToString().ToUpperInvariant() : value[0].ToString().ToLowerInvariant()) +
                       value.Remove(0, 1);
            }
            else
            {
                return value;
            }
        }
        public static bool HasExtention(this string value)
        {
            var ex = extentions.Concat(imageextentions);
            return ex.Contains(value.TrimAndToLower());
        }
        public static bool HasImageExtention(this string value)
        {
            var ex = imageextentions.Concat(imageextentions);
            return ex.Contains(value.TrimAndToLower());
        }

        public static int ToInt(this string value)
        {
            return int.Parse(value.Trim());
        }

        public static bool Is(this string value, string complareWith)
        {
            return value == complareWith;
        }
        public static decimal Evaluate(this string expression, decimal value)
        {
            //X*60/10
            if (string.IsNullOrEmpty(expression))
            {
                throw new InvalidDataException($"Expression cannot be null");
            }
            try
            {
                expression = expression.ToUpper().Trim().RemoveMiddleSpaces().Replace("X", value.ToPrice().ToString());
                DataTable table = new DataTable();
                table.Columns.Add("expression", typeof(string), expression);
                DataRow row = table.NewRow();
                table.Rows.Add(row);
                return decimal.Parse((string)row["expression"]).ToPrice();
            }
            catch (Exception e)
            {
                throw new InvalidDataException($"Invalied expression {expression}");
            }
        }
        public static bool IsEmail(this string email)
        {
            return Regex.IsMatch(email,
           @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
           RegexOptions.IgnoreCase);
        }
        public static string BEncrypt(this string clearText)
        {
            string mySalt = BCrypt.Net.BCrypt.GenerateSalt();
            return BCrypt.Net.BCrypt.HashPassword(clearText, mySalt);
        }

        public static void TryConvertLatLon(this string item)
        {
            if (!string.IsNullOrWhiteSpace(item))
            {
                if (!decimal.TryParse(item, out var r))
                {
                    throw new InvalidDataException("string cannot convert lat log");
                }
            }
        }

        public static bool IsValiedPhoneNumber(this string phoneNumber, out string formatPhoneNumber)
        {
            formatPhoneNumber = string.Empty;
            if (phoneNumber.Length.Is(10) && phoneNumber.StartsWith("07") && decimal.TryParse(phoneNumber, out var d))
            {
                formatPhoneNumber = $"94{phoneNumber.Remove(0, 1)}";
                return true;
            }
            else if (phoneNumber.Length.Is(11) && phoneNumber.StartsWith("94") && decimal.TryParse(phoneNumber, out var d2))
            {
                formatPhoneNumber = phoneNumber;
                return true;
            }
            return false;
        }

        public static string SubStringByIndexs(this string value, int indexFrom, int indexTo)
        {
            try
            {
                return value.Substring(indexFrom, (indexTo - indexFrom) + 1);
            }
            catch (Exception e)
            {
                throw new InvalidDataException("invalied substring arguments");
            }
        }

        public static string MergeString(this string value, int mergeFromindex, string mergeValue)
        {
            var charArray = value.ToCharArray().ToList();
            var mergeValueCharCount = 0;
            for (int i = 0; i < charArray.Count; i++)
            {
                if (mergeFromindex <= i)
                {
                    try
                    {
                        charArray[i] = mergeValue[mergeValueCharCount];
                        mergeValueCharCount++;
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                }
            }
            return new string(charArray.ToArray());
        }
        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}
