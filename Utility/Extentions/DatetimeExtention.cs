﻿using System;
using System.Globalization;

namespace Inx.Utility.Extentions
{
    public static class DatetimeExtention
    {
        public static DateTime GetMinDateTime(this DateTime value)
        {
            value = new DateTime(1970,1,2);
            return value;
        }
        public static DateTime GetMaxDateTime(this DateTime value)
        {
            value = new DateTime(2100, 1, 1);
            return value;
        }
        public static DateTime DefaultDateTime(this DateTime value)
        {
            return value. GetMinDateTime();
        }

        public static int GetAge(this DateTime birthday)
        {
            return Convert.ToInt32((DateTime.Today - birthday).TotalDays / 365);
        }
        public static int GetDeferent(this DateTime date,DateTime with)
        {
            return Convert.ToInt32((with - date).TotalDays);
        }
        public static int GetDeferentMinutes(this DateTime date, DateTime with)
        {
            return Convert.ToInt32((with - date).TotalMinutes);
        }
        public static DateTime ToTime(this DateTime datetime)
        {
            return new DateTime(1970, 1, 1, datetime.Hour, datetime.Minute, 0);
        }

        public static int ToIntDate(this DateTime datetime)
        {
            //20180202
            Func<int, string> formatter = (e) => { return e.ToString().Length == 1 ? $"0{e}" : e.ToString(); };
            return Convert.ToInt32 ($"{datetime.Year}{formatter(datetime.Month)}{formatter(datetime.Day)}");
        }
        public static DateTime IntToDateTime(this int datetimeInt)
        { 
            try
            {
                var y = Convert.ToInt32(datetimeInt.ToString().Substring(0, 4));
                var m = Convert.ToInt32(datetimeInt.ToString().Substring(4, 2));
                var d = Convert.ToInt32(datetimeInt.ToString().Substring(6, 2));
                return new DateTime(y, m, d);
            }
            catch (Exception e)
            {
                throw new InvalidCastException("invalied datetimeint");
            }
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="compare">Check is this date future</param>
       /// <returns></returns>
        public static bool IsFutureDate(this DateTime dateTime,DateTime compare,int dates=0)
        {
            dateTime.AddDays(dates);
            return (compare - dateTime).TotalDays > 0;
        }

        public static bool IsDateEqual(this DateTime date, DateTime compareWith)
        {
            return date.Day == compareWith.Day;
        }

        public static string To24HourTime(this DateTime date)
        {
            return 
                DateTime.ParseExact(date.ToShortTimeString(), "h:mm tt", CultureInfo.InvariantCulture).ToString("HH:mm");
        }

        public static DateTime ToLocalDateTime(this DateTime datetimeUtc, string toTimeZone)
        {
            var tzi = TimeZoneInfo.FindSystemTimeZoneById(toTimeZone);
            return TimeZoneInfo.ConvertTimeFromUtc(datetimeUtc, tzi);
        }

        public static DateTime SpecifiedAsUtc(this DateTime unspecified)
        {
           return DateTime.SpecifyKind(unspecified, DateTimeKind.Utc);
        }
    }
}
