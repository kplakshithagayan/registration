﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

namespace Inx.Utility.Extentions
{
    public static class ImageExtention
    {
        #region old
        public static Stream ImageConvertor(this Stream source, string size = "150x150", int quality = 75)
        {
            return source.ImageConvertor(int.Parse(size.Split('x')[0]), int.Parse(size.Split('x')[1]), quality);
        }

        public static Stream ImageConvertor(this Stream source, int width = 150, int height = 150, int quality = 75)
        {
            return source;
            var ms = new MemoryStream();
            var image = new Bitmap(source);
            var newImage = new Bitmap(image, width, height);
            newImage.Save(ms, ImageFormat.Png);
            return ms;
        }
        public static Stream SavePng(this Bitmap source, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");

            // Encoder parameter for image quality 
            var qualityParam = new EncoderParameter(Encoder.Quality, quality);
            // JPEG image codec 
            var jpegCodec = GetEncoderInfo("image/png");
            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            Stream s = null;
            source.Save(s, jpegCodec, encoderParams);
            return s;
        }

        /// <summary> 
        /// Returns the image codec with the given mime type 
        /// </summary> 
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // GetAsync image codecs for all image formats 
            var codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (var i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];

            return null;
        }

        #endregion

        public static Image Resize(this Stream source, int maxWidth, int maxHeight)
        {
            var current = new Bitmap(source);
            int width, height;
            #region reckon size
            if (current.Width > current.Height)
            {
                width = maxWidth;
                height = Convert.ToInt32(current.Height * maxHeight / (double)current.Width);
            }
            else
            {
                width = Convert.ToInt32(current.Width * maxWidth / (double)current.Height);
                height = maxHeight;
            }
            #endregion

            #region get resized bitmap
            var canvas = new Bitmap(width, height);

            using (var graphics = Graphics.FromImage(canvas))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(current, 0, 0, width, height);
            }

            return canvas;
            #endregion
        }

        public static byte[] ToByteArray(this Image current)
        {
            using (var stream = new MemoryStream())
            {
                current.Save(stream, current.RawFormat);
                return stream.ToArray();
            }
        }
    }
}
   
 
