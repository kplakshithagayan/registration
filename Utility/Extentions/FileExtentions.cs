﻿using System;
using System.IO;

namespace Inx.Utility.Extentions
{
    public static class FileExtentions
    {
        public static MemoryStream ToMemoryStream(this string base64encodedstring)
        {
            var bytes = Convert.FromBase64String(base64encodedstring);
            return new MemoryStream(bytes);
        }
    }
}
