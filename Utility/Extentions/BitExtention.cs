﻿using System.Text;
namespace Inx.Utility.Extentions
{
    public static class BitExtention
    {
        public static string ToBitString(this string bits,int length)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                sb.Append("0");
            }
            return sb.ToString();
        }
    }
}
