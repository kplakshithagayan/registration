﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Newtonsoft.Json;

namespace Inx.Utility.Extentions
{
    public static class ObjectExtention
    {
        public static bool IsNull(this object value)
        {
            return value == null;
        }
        public static string ToJsonString(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static string CacheKeyGen(this List<string> keys)
        {
            var key = new StringBuilder();
            if (keys == null || keys.Count == 0)
            {
                return string.Empty;
            }
            key.Append("_");
            foreach (var item in keys)
            {
                key.Append($"{item}_");
            }
            return key.ToString();
        }
        public static T DeepCopy<T>(this T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        public static bool IsValiedModelState<T>(this T obj,out List<ValidationResult> outResult)
        {
            outResult = new List<ValidationResult>();
            var validationContext = new System.ComponentModel.DataAnnotations.ValidationContext(obj);
             
            return Validator.TryValidateObject(obj, validationContext, outResult);
        }
    }
}
