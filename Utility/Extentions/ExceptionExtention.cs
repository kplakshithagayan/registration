﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Inx.Utility.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Inx.Utility.Extentions
{
    public static class ExceptionExtention
    {
        public static Exception HandleException(this Exception ex, object requestObject)
        {
            var stringObject = string.Empty;
            if (!requestObject.IsNull())
            {
                try
                {
                    stringObject = requestObject.ToJsonString();
                }
                catch (Exception e)
                {
                    stringObject = "";
                }               
            }
            throw HandleException(ex, stringObject);
        }

        public static Exception HandleException(this Exception ex,string requestObject="")
        {
            ex = ex.GetBaseException();
            if (ex is SqlException)
            {
                var e = ex as SqlException;
                switch (e.Number)
                {
                    case 2627:
                    case 2601:
                        return new PrimaryKeyViolationException();
                    default:
                        if (new List<int>() { 2601, 1062 }.Contains(e.Number))
                        {
                            return new PrimaryKeyViolationException();
                        }

                        break;
                }
                return ex;
            }
            else if (ex is DbUpdateException)
            {
                return new EntityValidationException(ex.GetBaseException().Message);
            }
            else if (ex is ArgumentNullException)
            {
                return new RecordNotFoundException();
            }
            else if (ex is ForbiddenException ||
                     ex is UnAuthorizedException ||
                     ex is RecordNotFoundException ||
                     ex is NullObjectException ||
                     ex is EntityValidationException ||
                     ex is PrimaryKeyViolationException ||
                     ex is OperationFailedException ||
                     ex is InvalidProcessException ||
                     ex is InvalidDataException ||
                     ex is DataInconsistencyException ||
                     ex is DataInconsistencyClientException ||
                     ex is EatException 
                     )
            {
                throw ex;
            }
            else
            {
                return new InternalSrverErrorException(ex, requestObject);
            }
        }

        public static string GetFullErrorText(this DbUpdateException exc)
        {
            //TODO
            return "todo";
        }

        public static bool IgnoreException(this Exception e)
        {
            switch (e)
            {
                case ForbiddenException _:
                    return true;
                case UnAuthorizedException _:
                    return true;
                case RecordNotFoundException _:
                    return true;
                case InvalidProcessException _:
                    return true;
                case InvalidDataException _:
                    return true;
                case NullObjectException _:
                    return true;
                    return true;
                default:
                    return false;
            }
        }
    }
}
