﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Inx.Utility.Extentions
{
    public static class ArrayExtention
    {
        public static bool TryGet(this string[] arr, int index, out string value)
        {
            try
            {
                value = arr[index];
                return true;
            }
            catch (Exception e)
            {
                value = string.Empty;
                return false;
            }
        }

        public static bool IsNullOrZero<T>(this List<T> arr)
        {
            if (arr.IsNull() || arr.Count == 0)
            {
                return true;
            }
            return false;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var seenKeys = new HashSet<TKey>();
            foreach (var element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.PickRandom(1).Single();
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }

        public static List<string> GetValiedEmails(this List<string> emails)
        {
            var valiedEmails = new List<string>();
            emails.ForEach(item =>
            {
                if (item.IsEmail())
                {
                    valiedEmails.Add(item);
                }
            });
            return valiedEmails.Distinct().ToList();
        }

        public static bool IsElementsEquals<T>(this List<T> inputArray, List<T> compareWith, out List<T> missingInInputArray,
            out List<T> addedToInputArray)
        {
            var fresult = false;
            missingInInputArray = new List<T>();
            addedToInputArray = new List<T>();
            if (inputArray.IsNullOrZero() && compareWith.IsNullOrZero())
            {
                fresult = true;
            }
            if (!inputArray.Count.Is(compareWith.Count))
            {
                fresult = false;
            }

            foreach (var item in inputArray)
            {
                if (!compareWith.Contains(item))
                {
                    missingInInputArray.Add(item);
                }
            }
            foreach (var item in compareWith)
            {
                if (!inputArray.Contains(item))
                {
                    addedToInputArray.Add(item);
                }
            }

            fresult = missingInInputArray.IsNullOrZero() && addedToInputArray.IsNullOrZero();

            return fresult;
        }
        public static IEnumerable<T> ToIEnumerable<T>(this IEnumerator<T> enumerator)
        {
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current;
            }
        }
    }
}
