﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Inx.Utility.Extentions
{
    public static class SeatAvailabilityExtentions
    {

        public static bool IsScheduleSeatsAvailable(this List<KeyValuePair<long, string>> item,string availabilityPatten)
        {
            if (string.IsNullOrEmpty(availabilityPatten))
            {
                return false;
            }
            var first = availabilityPatten.IndexOf("0", StringComparison.Ordinal);
            var last = availabilityPatten.LastIndexOf("0", StringComparison.Ordinal);
            foreach (var i in item)
            {
                if (!i.Value.Substring(first, (last - first + 1)).ToCharArray().Contains('1'))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
