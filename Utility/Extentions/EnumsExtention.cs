﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Inx.Utility.Models;

namespace Inx.Utility.Extentions
{
    public static class EnumsExtention
    {
        public static List<EnumMember> GetList<T>() where T : IConvertible
        {
            var type = typeof(T);

            if (!type.GetTypeInfo().IsEnum)
            {
                throw new ArgumentException("T must be of type enumeration.");
            }

            return Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(e => new EnumMember()
                {
                    Text = e.ToString(),
                    Value = ((IConvertible)e).ToInt32(null),
                    DisplayName = e.GetDescription()
                }).ToList();

        }

        private static string GetDescription(this object value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }

        public static string GetDescription(this Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }
        public static string TrimAndToLower(this Enum value)
        {
            return
                value.ToString().TrimAndToLower();
        }
        public static string ToCacheKeyValue(this Enum value)
        {
            return $"{value.ToString()}KeyValue";
        }
        public static int PropertyCount(this Enum value)
        {
            return Enum.GetNames(value.GetType()).Length;
        }
    }
}
