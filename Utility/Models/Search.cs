﻿using System;

namespace Inx.Utility.Models
{
    [Serializable]
    public class SearchRequest
    {
        public string OrderBy { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public string Search { get; set; }
        public int TenantId { get; set; }
        public string SortOrder { get; set; }
        public string OrderAndSort { get { return OrderBy + " " + SortOrder; } }
        public SearchRequest()
        {
            Skip = 0;
            Take = 0;
            TenantId = 0;
            Search = "";
            OrderBy = "all";
            SortOrder = "asc";
        }
    }
}
