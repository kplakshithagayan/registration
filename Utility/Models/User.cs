﻿using static Inx.Utility.Utility.BaseEnums;

namespace Inx.Utility.Models
{
    public class User
    {
        public long UserId { get; set; }
        public UserType UserType { get; set; }
        public int? TenantId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
