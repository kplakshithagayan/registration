﻿ namespace Inx.Utility.Models
{
    public class Logg
    {
        public int? TenantId { get; set; }
        public int? UserId { get; set; }
        public string SesseionId { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }

        public Logg()
        {
        }
        public Logg(int? tenant,int? userid,string sessionId,string stacktrace,string message="")
        {
            TenantId = tenant;
            UserId = userid;
            SesseionId = sessionId;
            StackTrace = stacktrace;
            Message = message;
        }
    }
}
