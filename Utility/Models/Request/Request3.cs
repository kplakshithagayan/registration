﻿using System;
using static Inx.Utility.Utility.BaseEnums;

namespace Inx.Utility.Models.Request
{
    [Serializable]
    public class Request<T1, T2, T3>
    {
        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }
        public T3 Item3 { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }
        public UserType UserType { get; set; }

        public Request()
        {
        }

        public Request(T1 item1, T2 item2, T3 item3, User user)
        {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
            UserId = user.UserId;
            UserType = user.UserType;
            TenantId = user.TenantId;
        }

        public User User
        {
            get
            {
                return new User()
                {
                    UserId = UserId,
                    UserType = UserType,
                    TenantId = TenantId
                };
            }
        }
    }
}