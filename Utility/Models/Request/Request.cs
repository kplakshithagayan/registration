﻿using System;
using static Inx.Utility.Utility.BaseEnums;

namespace Inx.Utility.Models.Request
{
    [Serializable]
    public class Request<T1>
    {
        public T1 Item { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }
        public UserType UserType { get; set; }
        public Request()
        {
        }
        public Request(T1 item,User user)
        {
            Item = item;
            UserId = user.UserId;
            UserType = user.UserType;
            TenantId = user.TenantId;
        }
        public User User
        {
            get
            {
                return new User()
                {
                    UserId = UserId,
                    UserType = UserType
                };

            }
        }
    }
}
