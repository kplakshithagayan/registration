﻿namespace Inx.Utility.Models
{
    public class EnumMember
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public string DisplayName { get; set; }
    }
}
