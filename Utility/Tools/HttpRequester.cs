﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Inx.Utility.Exceptions;
using Inx.Utility.Extentions;

// https://stackoverflow.com/questions/27108264/c-sharp-how-to-properly-make-a-http-web-get-request/27108442
namespace Inx.Utility.Tools
{
    public class HttpRequester
    {
        private readonly string _url;
        public HttpRequester(string url)
        {
            _url = url;
        }

        public string GetRequest()
        {
            try
            {
                var html = string.Empty;
                var url = _url;
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                return html;
            }
            catch (Exception e)
            {
                throw new EatException(e);
            }
        }

        public async Task<T> PostRequest<T>(List<KeyValuePair<string, string>> headers = null, string body = "")
        {
            try
            {
                using (var client = new HttpClient())
                {
                    if (!headers.IsNullOrZero())
                    {
                        client.DefaultRequestHeaders.Add(headers[0].Key, headers[0].Value);
                        client.DefaultRequestHeaders.Add(headers[1].Key, headers[1].Value);
                    }

                    HttpContent content = new StringContent(body, Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = await client.PostAsync(_url, content);
                    var resultContent = await response.Content.ReadAsStringAsync();
                    response.EnsureSuccessStatusCode(); 
                    var responseObject = resultContent.ToJsonObject<T>();
                    return responseObject;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}