﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Utility.Extentions;

namespace Inx.Utility.Attributes
{
    public class FutureDateAttribute : ValidationAttribute
    {
        private DateTime CompareWithDate { get; set; }
        public FutureDateAttribute()
        {
            CompareWithDate = DateTime.UtcNow;
        }
        public FutureDateAttribute(DateTime compareWithDate)
        {
            CompareWithDate = compareWithDate;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!value.IsNull() && Convert.ToDateTime(value) < CompareWithDate)
            {
                return new ValidationResult("date must be future");
            }
            return null;
        }
    }
}
