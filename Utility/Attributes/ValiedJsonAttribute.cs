﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Utility.Extentions;

namespace Inx.Utility.Attributes
{
    public class ValiedJsonAttribute : ValidationAttribute
    {
        public Type CheckType { get; set; }
 
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return !value.ToString().ValiedJson() ? new ValidationResult("this attibute must value with gratter than zero") : null;
        }
    }
}
