﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Utility.Extentions;

namespace Inx.Utility.Attributes
{
    public class PastDateAttribute: ValidationAttribute
    {
        private DateTime CompareWithDate { get; set; }
        public PastDateAttribute()
        {
            CompareWithDate = DateTime.UtcNow;
        }
        public PastDateAttribute(DateTime compareWithDate)
        {
            CompareWithDate = compareWithDate;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!value.IsNull() && Convert.ToDateTime(value) > CompareWithDate)
            {
                return new ValidationResult("date must be past.");
            }
            return null;
        }
    }
}
