﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Inx.Utility.Attributes
{
    public class NumberNotZeroAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (Convert.ToInt32(value) <= 0 || value == null)
            {
                return new ValidationResult("this attibute must value with gratter than zero");
            }
            return null;    
        }
    }
}
