﻿using System;
using System.ComponentModel.DataAnnotations;
using Inx.Utility.Extentions;
namespace Inx.Utility.Attributes
{
    public class ValiedDateIntAttribute : ValidationAttribute
    {
        public bool? IsFuture { get; set; }
        public ValiedDateIntAttribute()
        {
            IsFuture = null;
        }
        public ValiedDateIntAttribute(bool? isFuture=null)
        {
            IsFuture = isFuture;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var dateInt = Convert.ToInt32(value);
                var date = dateInt.IntToDateTime();                
                return null;
            }
            catch (Exception e)
            {
                return new ValidationResult("invalied date int");
            }
        }
    }
}