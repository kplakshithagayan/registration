﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Inx.Utility.Extentions;

namespace Inx.Utility.Attributes
{
    public class PhoneNumberAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    return null;
                }
                if (!value.ToString().IsValiedPhoneNumber(out var formattedPhoneNumber))
                {
                    throw new InvalidDataException("Phone number must" +
                                                   "01) start with '07' or '947'." +
                                                   "02) have 10(if 07) or 11(if 947)." +
                                                   "03) must have numaric values.");
                }
                return null;
            }
            catch (Exception e)
            {
                return new ValidationResult("invalied phone number");
            }
        }
    }
}
