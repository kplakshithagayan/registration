﻿using System.ComponentModel.DataAnnotations;
using Inx.Utility.Extentions;

namespace Inx.Utility.Attributes
{
    public class ObjectNotNullAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return value.IsNull() ? new ValidationResult("this object cannot be null") : null;
        }
    }
}