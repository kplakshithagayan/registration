﻿using System.ComponentModel.DataAnnotations;

namespace Inx.Utility.Attributes
{
    public class IfStringRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return value == null ? new ValidationResult("value required") : null;
        }
    }
}
