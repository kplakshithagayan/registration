﻿namespace Inx.Utility.Utility
{
    public class Constants
    {
        public const string OrganizationNoImage = "noorganizationlogo.png";
        public const string ProfileNoImage = "noprofile.png";
        public const string DefaultUserDisplayName = "Jawis";
        public const string SuperAdmin = "superadmin";
        public const string DefaultImage = "no.png";
        public const string NewLine = "%0D%0A";
    }
}
