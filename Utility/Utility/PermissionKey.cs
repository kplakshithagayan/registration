﻿namespace Inx.Utility.Utility
{
    public class PermissionKey
    {
        public enum Keys
        {
            //agent
            Agent_Crate = 1,
            Agent_Update = 2,
            Agent_Disable = 3,
            Agent_Read = 4,
            //agent user
            AgentUser_Create = 10,
            AgentUser_Update = 11,
            AgentUser_ResetPassword = 12,

            AgentUser_AuthorizePayment = 20
        }
    }
}
