﻿namespace Inx.Utility.Utility
{
    public class BaseEnums
    {
        public enum UserType
        {
            None = 0,
            CompanyUser = 1,
            AgentUser = 2,
            CustomerUser = 3,
            SubAgentUser = 4
        }

        public enum Title
        {
            Mr = 1,
            Mrs,
            Ms,
            Miss
        }

        public enum AccountStatus
        {
            Active = 1,
            Disabled
        }
    }
}
