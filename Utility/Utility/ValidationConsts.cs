﻿namespace Inx.Utility.Utility
{
    public class ValidationConsts
    {
        public const int NameLength = 50;
        public const int ContactNumberLength = 50;
        public const int UserNameMaxLength = 50;
        public const int PasswordMaxLength = 10;
        public const int DescriptionLength = 500;
        public const int JsonStringLength = 4000;
        public const int EmailLength = 50;
        public const int UrlLength = 200;
        public const int IdentityLength = 15;
        public const string EmailRegularExpression = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        public const string UserRegularExpression = @"^[a-zA-Z][a-zA-Z0-9-_\.]{7,}$";
        public const string PasswordRegularExpression = @"(?=^.{7,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";
    }
}
